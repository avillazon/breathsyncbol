# BreathSyncBOL

A system to synchronize the breath for ventilators (for covid-19). This project is aimed to help open-source ventitalors development. 
The system detects cough using different sensors, and sends a signal to re-synchronize the ventilator.

## Getting started

The BreathSyncBOL system allows to detect patient's breath and cough movements with different sensors. The received signals are processed to detect cough, using an algorithm that processes a customizable amount of movement signal (e.g. a block of 2 seconds breath signal taken every 200 ms). The algorithm detects cough movements, based on training signals measured with the different sensors. 

<img src="images/CoughDetected.png" width="50%" height="50%">

The algorithm uses a threshold to detect cough, and send a signal to the ventilator to re-synchornize the breath. The image show the breath signal and the detected cough, through irregular breath movements.


## Prerequisites

The project should be run in a low-cost [RaspberryPi mini-computer](https://www.raspberrypi.org/) with `python3` installed. 

Breath detection can be done with 4 different sensors:

  - An accelerometer from any Android smartphone with the free [SensorStreamer App](https://play.google.com/store/apps/details?id=cz.honzamrazek.sensorstreamer&hl=en) streaming data from a Smartphone over a TCP/IP (Requires a wifi network)

  - A MPU-6050 accelerometer directly connected to the RPi3

  - A Humidity and Temperature sensor (DHT11) connected to the RPi3

  - A piezoelectric sensor (under development)


## Installing the RPi3

Connect you RPi3 to a Wifi or Ethernet network, and install `python3`.

```
sudo apt-get update && sudo apt-get upgrade

sudo apt-get install python3
```

Now, clone the BreathSyncBOL repository on the RPi3:

```
git clone git@gitlab.com:avillazon/breathsyncbol.git
```

You will find the following subdirectories:

- `data` : The data used to calibrate the algorithm. This data was collected during 1 hour using an accelerometer in 3 different settings:

	- `baseline` : normal breathing, coughing every 10 minutes
	- `deep` : deep breathing, coughing every 10 minutes
	- `shalow`: shallow breathing, coughing every 10 minutes

- `python` : server to collect data from SensorStreamer App, the  MPU-6050 sensor, the humidity and the temperature sensors, or the piezoelectric sensor.

## Collecting breath data

The data collection can be done using the following options:

### Option A) Installing and setting the SensorStreamer App

In this option, a smartphone is used to collect data from the patient. The smartphone is placed in the breast of the patient, and fixed with a tape to avoid moving.

Once you have installed the [SensorStreamer App](https://play.google.com/store/apps/details?id=cz.honzamrazek.sensorstreamer&hl=en) on an Android smartphone, you need to create the communication settings to communicate with the RPi3.

For this, create a new connection and packet on the App as follow:

On the top left menu, select "Manage Connections" 

<img src="images/ManageConnections.png" width="25%" height="25%">

Create a new connection, with some name (e.g. ToRPi3) and select TCP Client, giving the `hostname` of the RPi3 (execute `hostname -I` on a terminal on the RPi3 to know its IP address) and the port `4321`, and save the connection:

<img src="images/NewConnection.png" width="25%" height="25%">

On the top left menu, select "Manage Packets": 

<img src="images/ManagePackets.png" width="25%" height="25%">

Create a new packet, called `accelerometer`, select `JSON format`, which includes `timestamp` and select `Accelerometer` data. Save the packet setting:

<img src="images/NewPacket.png" width="25%" height="25%">

### Option B) Installing and setting the MPU-6050 accelerometer

With this option, the MPU-6050 sensor must be connected with cables to the RPi3, and be placed in the breast of the patient (using a tape to prevent the sensor to move). 

Before connecting the pins, you must enable the `I2C` settings on the RPi3. 

```
sudo raspi-config
```

The selecting the `Interfacing Options` and finally `I2C` and rebooting the RPi3.


Then, connect the MPU-6050 accelerometer as follows:

<img src="images/MPU6050_RPi.png" width="50%" height="50%">

| RPi3 (PIN) | MPU-6050 |
|:---:|:---:|
| 5V (2) | VCC |
| GND (34) | GND |
| I2C-SLC (5) | SCL |
| I2C-SDA (3) | SDA |


### Option C) Installing and setting the humidity and temperature sensor (DHT11)

With this option, the sensor is placed inside the breathing mask of the patient. The breath is infered through temperature and humidity variations.




## Running the BreathSyncBOL's `bsync` server on RPi3

The `bsync` server can be used to collect data from the different sensors. The server receives different command line arguments to customize the data collection. 

Run `python3 bsync.py -h` to see the available options:

```
python3 bsync.py -h
usage: python3 bsync.py [-w <sample_win>] [-o <outputfile>] [-p <port>] [-v] [-i] [-c] [-h]
    -w : <sample_win> samples passed to cough detection
    -o : <outputfile> where sampling is written
    -p : <port> where data is received from client
    -v : verbose mode
    -i : IC2 mode reading from MPU6050
    -t : <ms> miliseconds between measurements (IC2 mode)
    -c : prints cough detection value
    -h : shows this message

```

### Option A) With the SensorStreamer App
To collect data using the accelerometer of the smartphone, start the python on the RPi3 (inside the `python` directory):

```
python3 bsync.py -o measurement.csv
```

Then, press "START" on the streamer on the SensorStream app. 


<img src="images/Stream.png" width="20%" height="20%">


You will start receiving the data, that will be stored in the `measurement.csv` file on the RPi3. You can stop the server with `Ctl-C` or stopping the stream on the SensorStream app ("STOP" button)

### Option B) With the MPU-6050 accelerometer

Once the MP6-6050 sensor placed on the patient, you can start data collections with:

```
python3 bsync.py -i -o measurement.csv
```

You can change the number of samples to analyze (sample window) with the `-w` option. The default value is 20 samples. 

You can change the sampling interval with the `-t` option (in miliseconds). The default value is 200 ms.

To see the cough detection value (`0` means no cough detected, and `1` cough detected), you can use the `-c` option. The cough value will be displayed, according to the `-t` value.

The output in CSV format is stored in the file passes as argument ot the `-o` option (silently). You can also print the values, by using the verbose `-v` option. 

### Option C) With the humidity and temperature sensor (DHT11)


## Acknowledgments

* [Universidad Privada Boliviana - UPB](http://www.upb.edu/) - Bolivia
* [Villca Solutions LLC](http://villca.com/) - United States
* [Quantum - Bolivia](https://tuquantum.com/) - Bolivia




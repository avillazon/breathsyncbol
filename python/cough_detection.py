#!/usr/bin/env python
# coding: utf-8

# In[ ]:

# Cough Detection Model
# Trained on 2020/04/09
# Villca Solutions LLC
def detect_cough(x, y, z, threshold=0.1):
    v2 = 0
    flag = 0
    for i in range(len(x)-1):
        v2 += (x[i+1]-x[i])**2 + (y[i+1]-y[i])**2 + (z[i+1]-z[i])**2
    if v2>threshold:
        flag = 1
    return flag

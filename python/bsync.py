import socket, threading
import json
import sys
import datetime
import getopt
import cough_detection as cd
import os
import time


class MPUThread(threading.Thread):
    def __init__(self,sample_win, output, verbose, ms, cough):
        threading.Thread.__init__(self)
        self.sample_win = sample_win
        self.output = output
        self.verbose = verbose
        self.ms = ms
        self.cough = cough

    def run(self):
        if os.uname()[4][:3] == 'arm':
            import mpu6050 as mpu
            f = open(self.output, 'w')
            x = []; y = []; z = []
            #start_time = time.time()
            while True:
                acc_x = mpu.read_raw_data(mpu.ACCEL_XOUT_H)
                acc_y = mpu.read_raw_data(mpu.ACCEL_YOUT_H)
                acc_z = mpu.read_raw_data(mpu.ACCEL_ZOUT_H)
                Ax = acc_x/16384.0
                Ay = acc_y/16384.0
                Az = acc_z/16384.0
                timestamp = int(time.time()*100000)
                newsample = "{},{:.8f},{:.8f},{:.8f}".format(timestamp,Ax,Ay,Az)
                x += [Ax]
                y += [Ay]
                z += [Az]
                if(len(x) >= self.sample_win):
                    if(self.cough):
                        print(cd.detect_cough(x,y,z), flush=True)
                    #elapsed_time = time.time() - start_time
                    x = []; y = []; z = []
                    #print(elapsed_time)
                    #start_time = time.time()
                if(self.verbose):
                    print(newsample, flush=True)
                f.write(newsample+'\n')
                time.sleep(self.ms/1000)
        else:
            print('Warning: -i (IC2 option) only available on RPi')

def getDataFromJSON(line, outfile, log=True):
    j = json.loads(line)
    timestamp = j['accelerometer']['timestamp']
    x = j['accelerometer']['value'][0]
    y = j['accelerometer']['value'][1]
    z = j['accelerometer']['value'][2]
    csv = "{},{},{},{}".format(timestamp,x,y,z)
    if log:
        print (csv, flush=True)
    outfile.write(csv+'\n')
    return [x,y,z]
    
# read data from SensorStream App in json format and
# converts to csv
class ClientThread(threading.Thread):
    def __init__(self,clientAddress,clientsocket, sample_win, output, verbose, cough):
        threading.Thread.__init__(self)
        self.csocket = clientsocket
        self.sample_win = sample_win
        self.output = output
        self.verbose = verbose
        self.cough = cough

    def run(self):
        msg = ''
        skip = 0
        count = 0
        prevjson = ''
        x = []; y = []; z = []
        f = open(self.output, 'w')
        while True:
            data = self.csocket.recv(2014) # buffer size
            if data:
                msg = data.decode()
                for line in msg.splitlines():
                    try:
                        newval = getDataFromJSON(line, f, log=self.verbose)
                        x += [newval[0]]
                        y += [newval[1]]
                        z += [newval[2]]
                        if(len(x) >= self.sample_win):
                            if(self.cough):
                                print(cd.detect_cough(x,y,z,threshold=2), flush=True)
                            x = []; y = []; z = []
                    except Exception as e: # rebuild incomplete samples (e.g. those that are incomplete in the socket)
                        #print(e)
                        if prevjson != '':
                            newjson = prevjson + line
                            for nline in newjson.splitlines():
                                try:
                                    newval = getDataFromJSON(nline, f, log=self.verbose)
                                    x += [newval[0]]
                                    y += [newval[1]]
                                    z += [newval[2]]
                                except Exception as ee:
                                    #print(ee) # Should not happen
                                    count += 1
                            prevjson = ''
                        else:
                            prevjson = line
                        skip += 1
        print ("Number of skip ", skip)

def usage():
    print('usage: python3', sys.argv[0], '[-w <sample_win>] [-o <outputfile>] [-p <port>] [-v] [-i] [-c] [-h]')
    print('    -w : <sample_win> samples passed to cough detection')
    print('    -o : <outputfile> where sampling is written')
    print('    -p : <port> where data is received from client')
    print('    -v : verbose mode')
    print('    -i : IC2 mode reading from MPU6050')
    print('    -t : <ms> miliseconds between measurements (IC2 mode)')
    print('    -c : prints cough detection value')
    print('    -h : shows this message')

def main():
    try:
        opts, args = getopt.getopt(sys.argv[1:], 'w:o:p:t:vich', ['window=', 'output=', 'port=', 'time='])
    except getopt.GetoptError as err:
        print(err)
        usage()
        sys.exit(2)

    sample_win = 20
    verbose = False # do not show on stdout
    port = 4321 # default port
    output = os.devnull
    spi = False
    ms = 200
    cough = False

    for o, a in opts:
        if o == "-v":
            verbose = True
        elif o in ("-w", "--window"):
            sample_win = int(a)
        elif o in ("-o", "--output"):
            output = a
        elif o in ("-p", "--port"):
            port = int(a)
        elif o in ("-t", "--time"):
            ms = int(a)
        elif o == "-i":
            spi = True
        elif o == "-c":
            cough = True
        elif o == "-h":
            usage()
            sys.exit()
        else:
            assert False, "unhandled option"

    if spi:
        mputhread = MPUThread(sample_win, output, verbose, ms, cough)
        mputhread.start()
    else:    
        server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        server.bind(('', port))

        while True:
            server.listen(1) # only expects 1 connection
            clientsock, clientAddress = server.accept()
            newthread = ClientThread(clientAddress, clientsock, sample_win, output, verbose, cough)
            newthread.start()

if __name__ == "__main__":
    main()

import mpu6050 as mpu
import time

while True:
    #Read Accelerometer raw value
    acc_x = mpu.read_raw_data(mpu.ACCEL_XOUT_H)
    acc_y = mpu.read_raw_data(mpu.ACCEL_YOUT_H)
    acc_z = mpu.read_raw_data(mpu.ACCEL_ZOUT_H)

    #Read Gyroscope raw value
    #gyro_x = read_raw_data(GYRO_XOUT_H)
    #gyro_y = read_raw_data(GYRO_YOUT_H)
    #gyro_z = read_raw_data(GYRO_ZOUT_H)

    #Full scale range +/- 250 degree/C as per sensitivity scale factor
    Ax = acc_x/16384.0
    Ay = acc_y/16384.0
    Az = acc_z/16384.0

    #Gx = gyro_x/131.0
    #Gy = gyro_y/131.0
    #Gz = gyro_z/131.0
    timestamp = int(time.time()*100000)
    print("{},{:.8f},{:.8f},{:.8f}".format(timestamp,Ax,Ay,Az))
    time.sleep(0.2)
